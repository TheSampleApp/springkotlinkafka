package com.the.sample.app

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SpringHibernateCrudApplication

fun main(args: Array<String>) {
    SpringApplication.run(SpringHibernateCrudApplication::class.java, *args)
}