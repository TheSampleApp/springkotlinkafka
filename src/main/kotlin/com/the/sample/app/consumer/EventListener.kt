package com.the.sample.app.consumer

import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class EventListener {
    @KafkaListener(topics = ["\${kafka.topic.name}"], groupId = "\${kafka.consumer.group}")
    fun onEvent(eventMessage: String?) {
        //process the event
    }
}