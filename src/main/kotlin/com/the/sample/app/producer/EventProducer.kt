package com.the.sample.app.producer
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component

interface EventProducer {
    fun produce(eventKey: String, eventValue: String): Unit
}
@Component
class EventProducerKafkaImpl(val kafkaTemplate: KafkaTemplate<String, String>,
                             @Value("\${kafka.topic.name}") val topicName: String) : EventProducer{

    override fun produce(eventKey: String, eventValue: String) {
        kafkaTemplate.send(topicName,eventKey,eventValue)
    }
}